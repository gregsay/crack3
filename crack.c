#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

#include <sys/types.h>
#include <sys/stat.h>

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings
const int SIZE = 100;   // length of dict

struct entry 
{
    // FILL THIS IN
    char *password;
    char *hash;
};

int by_hash1(const void *a, const void *b)// g comparison function for qsort
{
    return strcmp( (char *)a,(*(struct entry *)b).hash);
}

int by_hash(const void *a, const void *b)// g comparison function for qsort
{
    return strcmp((*(struct entry *)a).hash,(*(struct entry *)b).hash);
}


// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *guessHash = md5(guess, strlen(guess)); // g

    // Compare the two hashes
    // Free any malloc'd memory
    if (strcmp(guessHash, hash) == 0) // g
        {
            free (guessHash);
            return 1;
        }
    else
        {
            free (guessHash);
            return 0;
        }    
    

    
}

// Given a filename, return its length or -1 if error
int file_length(char *filename)
{
        struct stat fileinfo;// structure fileinfo using stat function.   Does this format file info to be like stat??
        if (stat(filename, &fileinfo) == -1)//  stats the file pointed to filename and fills in fileinfo"buffer" pointed to. "stat: is used to display file or file system status"
            return -1;// error
        else
            return fileinfo.st_size;// st_size field gives the size of the file in bytes.
            
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    
            // Obtain length of file{ 
        int len = file_length(filename);
        if (len == -1)
        {
            printf("Couldn't get length of file %s\n", filename);
            exit(1);
        }
        
        // Allocate memory for the entire file
        char *file_contents = malloc(len);
        
        // Read the entire file into file_contents
        FILE *fp = fopen(filename, "r");
        if (!fp)
        {
            printf("Couldn't open %s for reading\n", filename);
            exit(1);
        }
        fread(file_contents, 1, len, fp);// reads given stream fp(pointer to a file) into array file_contents(pointer to a block of memory with a minimum size of 1 * len bytes) 
        fclose(fp);
        
        // Replace \n  with \0
        int line_count =0;
        for (int i =0; i < len; i++)
        {
            if (file_contents[i] == '\n')
            {
                file_contents[i] = '\0';
                line_count++;
                
            }
        }
        
        struct entry *lines = malloc((line_count) * sizeof(struct entry));  
        
        int c = 0;
        for (int i = 0; i < line_count; i++)
        {
            lines[i].password = &file_contents[c];// lines is an array of structs to &file_contents
            
            // Scan forward to find  next line
            while (file_contents[c] != '\0') c++;
            c++;
        }
        
        
        // hash lines 
        for (int i = 0; i < SIZE ; i++)
            {
            lines[i].hash = md5(lines[i].password, strlen(lines[i].password));
            }
        
        free (file_contents);
        *size = line_count;
        return lines;
        free(lines);
        
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    int dlen;
    // TODO: Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary("rockyou100.txt", &dlen);// g
/*    
for (int i = 0; i < SIZE ; i++)
    {
        printf("  %s\n", dict[i].password);
        printf("  %s\n", dict[i].hash);
    }    
*/    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, SIZE, sizeof(struct entry), by_hash); //g qsort(array, length, size of element, comparison fn)
/*    
for (int i = 0; i < SIZE ; i++)
    {
        printf("%s\n", dict[i].password);
        printf("%s\n", dict[i].hash);
        
    }
*/    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    // Search through array using binary search
        // Open the hash file for reading.
    FILE *h;
    h = fopen("hashes.txt", "r");
    
    if (!h)
    {
        printf("Couldn't open hashes.txt\n");
        exit(1);
    }
    int d =0;
    struct entry *result;
    char hash [HASH_LEN];
    while(fgets(hash, HASH_LEN, h) != NULL)
    {
//printf("  %s\n", hash);
        result = bsearch(hash, dict, SIZE, sizeof(struct entry), by_hash1);   // (target, array cmp to, # of elements in array, compf)   
         
            if (result)
            {
                printf("Number %d\n",d);
                printf("Found password %s\n", result->password);
                printf("Found hash %s\n", result->hash);
                d++;
            }       
   
    }
    
    fclose(h);// close hasshes file
    
    //free(dict[0]);
    free(dict);
    
    
    
}
